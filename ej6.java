package ej3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ej6 extends JFrame {

	private JPanel contentPane;
	private JTextField A;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ej6 frame = new ej6();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ej6() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GREEN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("valorar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a�o;
				a�o=Integer.parseInt(A.getText());
				if (a�o==0) {
					A.setText("jardin de infantes");
				}else if (0<a�o & a�o<7 ) {
					A.setText("primaria");
				}	else if (7<=a�o & a�o<13) {
					A.setText("secundaria");
				}
			}
		
		});
		btnNewButton.setBounds(169, 136, 89, 23);
		contentPane.add(btnNewButton);
		
		A = new JTextField();
		A.setBounds(169, 77, 86, 20);
		contentPane.add(A);
		A.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("curso");
		lblNewLabel.setBounds(193, 35, 46, 14);
		contentPane.add(lblNewLabel);
	}

}
