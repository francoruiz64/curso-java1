package ej2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ejercicio2 extends JFrame {

	private JPanel contentPane;
	private JTextField A;
	private JTextField textField_1;
	private JTextField B;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ejercicio2 frame = new ejercicio2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ejercicio2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		A = new JTextField();
		A.setBackground(Color.LIGHT_GRAY);
		A.setBounds(30, 113, 86, 20);
		contentPane.add(A);
		A.setColumns(10);
		
		JButton btnResultado = new JButton("resultado");
		btnResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int variable;
				variable=Integer.parseInt(A.getText());
				if(variable % 2==0) {
					B.setText("PAR");
				}
			    else {
						B.setText("IMPAR");
					}
				}
			
		});
		btnResultado.setBounds(153, 112, 89, 23);
		contentPane.add(btnResultado);
		
		textField_1 = new JTextField();
		textField_1.setBounds(266, 125, 120, -12);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		B = new JTextField();
		B.setBackground(Color.LIGHT_GRAY);
		B.setBounds(276, 114, 86, 18);
		contentPane.add(B);
		B.setColumns(10);
		
		JLabel lblNumeroAIngresar = new JLabel("numero a ingresar");
		lblNumeroAIngresar.setBounds(30, 88, 140, 14);
		contentPane.add(lblNumeroAIngresar);
	}

}
