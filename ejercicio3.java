package ej3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ejercicio3 extends JFrame {

	private JPanel contentPane;
	private JTextField A;
	private JTextField B;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ejercicio3 frame = new ejercicio3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ejercicio3() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		A = new JTextField();
		A.setBounds(50, 105, 86, 20);
		contentPane.add(A);
		A.setColumns(10);
		
		JLabel lblNDelPuesto = new JLabel("n\u00B0 del puesto");
		lblNDelPuesto.setBounds(49, 80, 120, 14);
		contentPane.add(lblNDelPuesto);
		
		JButton btnObtencion = new JButton("obtencion");
		btnObtencion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int numero;
				numero=Integer.parseInt(A.getText());
				if(numero==1) {
					B.setText("felicidades ganaste la medalla de oro");
				} 
				if (numero==2) {
					B.setText("felicidades ganaste la medalla de plata");	
				}
				if(numero==3) {
					B.setText("felicidades ganaste la medalla de bronce");	
				}
				if(numero>3) {
					B.setText("segui participando looser");		
				}
			}
		});
		btnObtencion.setBounds(171, 104, 89, 23);
		contentPane.add(btnObtencion);
		
		B = new JTextField();
		B.setBounds(48, 149, 252, 20);
		contentPane.add(B);
		B.setColumns(10);
	}

}
