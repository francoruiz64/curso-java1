package ej1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ejercicio1 extends JFrame {

	private JPanel contentPane;
	private JTextField A;
	private JTextField B;
	private JTextField C;
	private JTextField D;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ejercicio1 frame = new ejercicio1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ejercicio1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GREEN);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		A = new JTextField();
		A.setBounds(60, 27, 86, 20);
		contentPane.add(A);
		A.setColumns(10);
	
		
		B = new JTextField();
		B.setBounds(60, 81, 86, 20);
		contentPane.add(B);
		B.setColumns(10);
		
		
		C = new JTextField();
		C.setBounds(60, 136, 86, 20);
		contentPane.add(C);
		C.setColumns(10);
	
		
		D = new JTextField();
		D.setBounds(297, 87, 127, 20);
		contentPane.add(D);
		D.setColumns(10);
		
		JLabel label = new JLabel("1\u00B0");
		label.setBounds(33, 30, 46, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("2\u00B0");
		label_1.setBounds(33, 84, 46, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("3\u00B0");
		label_2.setBounds(33, 142, 46, 14);
		contentPane.add(label_2);
		
		JButton boton = new JButton("CALCULAR");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
	    int a,b,c,d;
	    a=Integer.parseInt(A.getText());
	    b=Integer.parseInt(B.getText());
	    c=Integer.parseInt(C.getText());
	    d=((a+b+c)/3);
		if(d==7) {
			D.setText("APROBADO");
			
		} else {
			D.setText("DESAPROBADO");
		}
			}
		});
		boton.setBounds(176, 84, 106, 23);
		contentPane.add(boton);
	}
}
